#Make a figure that visualizes the major fungal lineages across the ICPF network

#We will do this seperately by depth increment (org vs. min)

#Clear environment
rm(list = ls(all.names = TRUE))

#load packages
require(dplyr)
require(vegan)
require(usethis)
require(phyloseq)
require(data.table)
require(ape)
require(ggplot2)
require(splitstackshape)
require(DESeq2)
require(cowplot)
require(ggrepel)

#load data paths
source('paths.R')

#Upload data files
otu.table <- as.data.frame(fread(otu.file.string, sep = '\t'))
icp.dat <- readRDS(icp.plot.level.envt.matrix.processed.string)
envt.mat <- read.csv(envt.file.string)
plot.dat <- readRDS(icp.plot.file.string)
ICP.tree.envt <- readRDS(individual.tree.growth.file.string)
fung.guild <- fread(funguild.file.string, sep = '\t')
taxonomy.table <- fread(taxonomy.reference.string, sep = '\t', header = FALSE)

#Now curate the raw otu table to remove files from the greenhouse experiment
otu.tab2 <- otu.table
row.names(otu.tab2) <- otu.tab2$`#OTU ID`
otu.tab2$`#OTU ID` <- NULL

#Pre-treatment greenhouse samples
otu.tab2$S_364 <- NULL
otu.tab2$S_365<- NULL
otu.tab2$S_366<- NULL
otu.tab2$S_367<- NULL
otu.tab2$S_368<- NULL
otu.tab2$S_369<- NULL
otu.tab2$S_370<- NULL
otu.tab2$S_371<- NULL
otu.tab2$S_372<- NULL
otu.tab2$S_373<- NULL
otu.tab2$S_374<- NULL

#Amanita rubescens DNA barcode
otu.tab2$S_376<- NULL

#And the pesky IM and LTER plots that made there way in + the Blanks
otu.tab2$S_4<- NULL
otu.tab2$S_11<- NULL
otu.tab2$S_292<- NULL
otu.tab2$S_293<- NULL
otu.tab2$S_325<- NULL
otu.tab2$S_329<- NULL
otu.tab2$S_340<- NULL
otu.tab2$S_358<- NULL

otu.tab2$X2.S_29_2019<- NULL
otu.tab2$X2.S_41_2019<- NULL
otu.tab2$S_115_2019<- NULL
otu.tab2$X2.S_54_2019<- NULL
otu.tab2$X2.S_102_2019<- NULL
otu.tab2$X2.S_107_2019<- NULL
otu.tab2$X2.S_108_2019<- NULL
otu.tab2$X2.S_109_2019<- NULL
otu.tab2$X2.S_110_2019<- NULL
otu.tab2$X2.S_111_2019<- NULL
otu.tab2$X2.S_112_2019<- NULL

#Remove the german sites that are not part of ICP
otu.tab2$S_15_2019<- NULL
otu.tab2$S_22_2019<- NULL
otu.tab2$S_29_2019<- NULL
otu.tab2$S_36_2019<- NULL
otu.tab2$S_46_2019<- NULL
otu.tab2$S_56_2019<- NULL
otu.tab2$S_71_2019<- NULL
otu.tab2$X2.S_57_2019<- NULL

#Now lets look at column sums
sort(colSums(otu.tab2))
otu.tab2 <- data.frame(t(otu.tab2))

#Filter out those with 500 sequences as this could be too great a range between min and max depth
high.depth.otu<- subset(otu.tab2, rowSums(otu.tab2) > 500)

#Now make the OTU table
high.depth.otu <- data.frame(t(high.depth.otu))

#Make taxonomy table
tax.tab <- data.frame(OTU = row.names(high.depth.otu))
tax.tab$Taxonomy <- taxonomy.table$V2[match(tax.tab$OTU, taxonomy.table$V1)]

fungal.otu.file.taxonomy <- data.frame(cSplit(tax.tab, 'Taxonomy', sep = ';'))

#Make taxonomy table
tax.tab <- data.frame(Kingdom = fungal.otu.file.taxonomy$Taxonomy_1,
                      Phylum = fungal.otu.file.taxonomy$Taxonomy_2,
                      Class = fungal.otu.file.taxonomy$Taxonomy_3,
                      Order = fungal.otu.file.taxonomy$Taxonomy_4,
                      Family = fungal.otu.file.taxonomy$Taxonomy_5,
                      Genus = fungal.otu.file.taxonomy$Taxonomy_6,
                      Spp = fungal.otu.file.taxonomy$Taxonomy_7)

#Now make the aggregations

#How many archaea?
kingdom.otu <- cbind(high.depth.otu, tax.tab$Kingdom)
kingdom.sum <- aggregate(. ~ `tax.tab$Kingdom`, data = kingdom.otu, FUN = 'sum')
row.names(kingdom.sum) <- kingdom.sum$`tax.tab$Kingdom`
kingdom.sum$`tax.tab$Kingdom` <- NULL

#Whats the phylum distribution?
phyla.otu <- cbind(high.depth.otu, tax.tab$Phylum)
phylum.sum <- aggregate(. ~ `tax.tab$Phylum`, data = phyla.otu, FUN = 'sum')
row.names(phylum.sum) <- phylum.sum$`tax.tab$Phylum`
phylum.sum$`tax.tab$Phylum` <- NULL

phyla.sum.plot <- data.frame(rowSums(phylum.sum))
phyla.sum.plot$rowSums.phylum.sum. <- 100*(phyla.sum.plot/colSums(phyla.sum.plot))
phyla.sum.plot <- phyla.sum.plot[order(-phyla.sum.plot$rowSums.phylum.sum),]
phyla.sum.plot.top10 <- data.frame(Phylum = row.names(phyla.sum.plot)[1:10], Abund = phyla.sum.plot[1:10,])
phyla.sum.plot.top10$Phylum <- gsub("p__", "", phyla.sum.plot.top10$Phylum)

fungal.phylum.plot <- ggplot(phyla.sum.plot.top10, aes(x = "", y = Abund, fill = reorder(Phylum, Abund))) + 
  geom_bar(stat = 'identity', width = 1)+
  coord_polar("y", start = 10)+
  labs(x = "", y = "Relative abundance (%)")+
  theme_void()+
  guides(fill=guide_legend(nrow=5, byrow=TRUE))+
  theme(legend.key.size = unit(0.2, 'cm'))+
  theme(legend.position = "bottom")+
  theme(plot.margin=grid::unit(c(0.2,0.2,0.2,0), "cm"))+ #T,r,b,l
  theme(legend.text = element_text(size = 6, color = "black"))+
  theme(legend.title = element_blank())+
  scale_fill_manual(values = c("#a6cee3", "#1f78b4", "#b2df8a", "#33a02c",
  "#fb9a99","#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", "#ffff99", "#b15928"))


#What's the genus distribtuion
#Whats the phylum distribution?
genus.otu <- cbind(high.depth.otu, tax.tab$Genus)
genus.sum <- aggregate(. ~ `tax.tab$Genus`, data = genus.otu, FUN = 'sum')
row.names(genus.sum) <- genus.sum$`tax.tab$Genus`
genus.sum$`tax.tab$Genus` <- NULL
genus.sum.plot <- data.frame(rowSums(genus.sum))
genus.sum.plot$rowSums.genus.sum. <- 100*(genus.sum.plot/colSums(genus.sum.plot))
genus.sum.plot <- genus.sum.plot[order(-genus.sum.plot$rowSums.genus.sum.),]
genus.sum.plot <- data.frame(Genus = row.names(genus.sum.plot)[1:10], Abund = genus.sum.plot[1:10,])
genus.sum.plot$Genus <- gsub("g__", "", genus.sum.plot$Genus)

