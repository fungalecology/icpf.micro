#Paths to the file directories are stored here

#Make the path to where all data is will be stored
root.path<- "/Users/marka.anthony/Documents/R_projects/ICPF_Soil_Microbiome/data_storage/"

#Now make the sub-directories for raw data-----
raw.path<- paste0(root.path,"raw_data/")

#And the files
otu.file.string<- paste0(raw.path,"feature-table.tsv")
envt.file.string<- paste0(raw.path,"Envt_matrix.csv")
individual.tree.growth.file.string<- paste0(raw.path,"Individual_tree_growth.rds")
icp.plot.file.string<- paste0(raw.path,"Plot_level_data.rds")
funguild.file.string <- paste0(raw.path, "Funguild.feed.taxa.guilds.txt")
taxonomy.reference.string <- paste0(raw.path, "sh_taxonomy_qiime_ver8_dynamic_10.05.2021.txt")
foliar.nutrients.file.string <- paste0(raw.path, "fo/fo_fom.csv")
tree.species.file.string <- paste0(raw.path, "Tree_species.csv")
allo.equations.file.string <- paste0(raw.path, "Allometric_equations.csv")
bacterial.otu.file.string <- paste0(raw.path, "Bacteria_feature-table.tsv")
bacterial.envt.file.string <- paste0(raw.path, "Bacteria_Envt_matrix.csv")
bacterial.taxonomy.reference.string <- paste0(raw.path, "Bacteria_taxonomy.tsv")
soil.ph.measurements.string <- paste0(raw.path, "ICPF_Soil_pH.csv")
fungal.traits.older.vs.string <- paste0(raw.path, "Fungal_traits_1_2_dec_2020.csv")
pfam.traits.string <- paste0(raw.path, "PFAM_traits.csv")
pfam.taxa.meta.data.string <- paste0(raw.path, "taxa_metadata.csv")
kegg.traits.string <- paste0(raw.path, "kegg_trait_080422.csv")
kegg.taxa.meta.data.string <- paste0(raw.path, "metadata_080422.csv")
soil.grids.data.string <- paste0(raw.path, "SoilGrids_ICP_forest_sampled.csv")
soil.org.matter.data.string <- paste0(raw.path, "321_so_20220223125907/so_som.csv")
plot.install.data.string <- paste0(raw.path, "si_sta.csv")
fleck.soil.data.string <- paste0(raw.path, "Soils_ICP_database.csv")
soil.prf.data.string <- paste0(raw.path, "so_prf.csv")

#Now make the subdirectories for data products----
product.path<- paste0(root.path,"data_products/")

#And the files
foliar.nutrients.processed.string <- paste0(product.path, "foliar.nutrients.rds")
community.alpha.beta.processed.string <- paste0(product.path, "alpha.beta.fungi.rds")
icp.plot.level.envt.matrix.processed.string <- paste0(product.path, "icp.plot.processed.rds")
exploration.type.1.processed.string <- paste0(product.path, "ecm.expl.type.1.rds")
exploration.type.2.processed.string <- paste0(product.path, "ecm.expl.type.2.rds")
exploration.type.3.processed.string <- paste0(product.path, "ecm.expl.type.3.rds")
exploration.type.4.processed.string <- paste0(product.path, "ecm.expl.type.4.rds")
bacterial.composition.raw.rare.string <- paste0(product.path, "bacteria.comp.raw.rare.rds")
bacterial.composition.prop.rare.string <- paste0(product.path, "bacteria.comp.prop.rare.rds")
bacterial.characteristics.string <- paste0(product.path, "bacterial.characteristics.rds")
kegg.pfam.make.save.string <- paste0(product.path, "pfam.kegg.cwm.rds")
ectos.kegg.pfam.make.save.string <- paste0(product.path, "ectos.pfam.kegg.cwm.rds")
saps.kegg.pfam.make.save.string <- paste0(product.path, "sap.pfam.kegg.cwm.rds")
icp.date.with.age.string <- paste0(product.path, "icp.data.age.rds")
biomass.growth.organic.horizon.microbiome.string <- paste0(product.path, "biomass.growth.org.rds")
biomass.growth.mineral.horizon.microbiome.string <- paste0(product.path, "biomass.growth.min.rds")
