#Look at the correlation between tree growth and soil carbon storage

#Clear environment
rm(list = ls(all.names = TRUE))

#load packages
require(dplyr)
require(ggplot2)
require(tidyr)
require(mgcv)
require(mgcv)
require(cowplot)
require(mgcv)
require(car)
require(plotly)
require(plot3D)
require(RSelenium)


#load data paths
source('paths.R')

#Upload the data files
icp.dat <- readRDS(icp.plot.level.envt.matrix.processed.string)
icp.dat.age <- readRDS(icp.date.with.age.string)
icp.envt <- read.csv(envt.file.string)
icp.envt$country_plot <- paste(icp.envt$Country, icp.envt$Plot, sep = '_')
soil.grids.data <- read.csv(soil.grids.data.string)
som.data <- read.csv(soil.org.matter.data.string, sep = ';')
som.data$country_plot <- paste(som.data$code_country, som.data$code_plot, sep = '_')
soil.ph.lab.meas <- read.csv(soil.ph.measurements.string) #Add the lab measured soil ph values
fleck.data <- read.csv(fleck.soil.data.string)
fleck.data$country_plot <- paste(fleck.data$code_country, fleck.data$code_plot, sep = '_')

#Now split these by soil depth
som.db.depth <- split(som.data, som.data$code_layer)
fleck.data.depth <- split(fleck.data, fleck.data$code_layer)

#Make VIF function
samvif <- function(mod){
  # mod is an mgcv object
  # this function calculates the variance inflation factors for GAM as no one else has written code to do it properly
  
  # this is used to summarise how well the GAM performed
  
  mod.sum <- summary(mod)
  s2 <- mod$sig2 # estimate of standard deviation of residuals
  X <- model.matrix(mod) # data used to fit the model
  n <- nrow(X) # how many observations were used in fitting?
  v <- -1 # omit the intercept term, it can't inflate variance
  varbeta <- mod.sum$p.table[v,2]^2 # variance in estimates
  varXj <- apply(X=X[,row.names(mod.sum$p.table)[v]],MARGIN=2, var) # variance of all the explanatory variables
  VIF <- varbeta/(s2/(n-1)*1/varXj) # the variance inflation factor, obtained by rearranging
  # var(beta_j) = s^2/(n-1) * 1/var(X_j) * VIF_j
  
  VIF.df <- data.frame(variable=names(VIF),
                       vif=VIF, 
                       row.names=NULL)
  
  return(VIF.df)
}

#Now make a df with country_plot and populate this with the depth variables
#key
#M01 = 0-10 cm
#M12 = 10-20 cm
#M24 = 20- 40 cm
#M48 = 40-80 cm
soc.tog <- data.frame(country_plot= unique(c(fleck.data$country_plot, som.data$country_plot)))

M01 = data.frame(SOC = c(som.db.depth$M01$organic_carbon_total, fleck.data.depth$M01$organic_carbon_total), 
                 country_plot = c(som.db.depth$M01$country_plot, fleck.data.depth$M01$country_plot),
                 BD = c(som.db.depth$M01$bulk_density, fleck.data.depth$M01$bulk_density),
                 Coarse = c(som.db.depth$M01$coarse_fragment_vol, fleck.data.depth$M01$coarse_fragment_vol))
M12 = data.frame(SOC = c(som.db.depth$M12$organic_carbon_total, fleck.data.depth$M12$organic_carbon_total), 
                 country_plot = c(som.db.depth$M12$country_plot, fleck.data.depth$M12$country_plot),
                 BD = c(som.db.depth$M12$bulk_density, fleck.data.depth$M12$bulk_density),
                 Coarse = c(som.db.depth$M12$coarse_fragment_vol, fleck.data.depth$M12$coarse_fragment_vol))
M24 = data.frame(SOC = c(som.db.depth$M24$organic_carbon_total, fleck.data.depth$M24$organic_carbon_total), 
                 country_plot = c(som.db.depth$M24$country_plot, fleck.data.depth$M24$country_plot),
                 BD = c(som.db.depth$M24$bulk_density, fleck.data.depth$M24$bulk_density),
                 Coarse = c(som.db.depth$M24$coarse_fragment_vol, fleck.data.depth$M24$coarse_fragment_vol))
M48 = data.frame(SOC = c(som.db.depth$M48$organic_carbon_total, fleck.data.depth$M48$organic_carbon_total), 
                 country_plot = c(som.db.depth$M48$country_plot, fleck.data.depth$M48$country_plot),
                 BD = c(som.db.depth$M48$bulk_density, fleck.data.depth$M48$bulk_density),
                 Coarse = c(som.db.depth$M48$coarse_fragment_vol, fleck.data.depth$M48$coarse_fragment_vol))

#Add the organic horizon
OFH = data.frame(SOC = c(som.db.depth$OFH$organic_carbon_total, fleck.data.depth$OFH$organic_carbon_total), 
                 country_plot = c(som.db.depth$OFH$country_plot, fleck.data.depth$OFH$country_plot),
                 Weight = c(som.db.depth$OFH$organic_layer_weight, fleck.data.depth$OFH$organic_layer_weight))


OFH$OFH.SOC <- ((0.1*OFH$SOC)*OFH$Weight)*0.1 #% C x mass  (kg/m2) so multiply by 0.1 to get to t/ha
OFH$OFH.SOC <- replace(OFH$OFH.SOC, which(OFH$OFH.SOC < 0), NA)

M01.agg <- aggregate(. ~ country_plot, FUN = mean, data = M01)
M12.agg <- aggregate(. ~ country_plot, FUN = mean, data = M12)
M24.agg <- aggregate(. ~ country_plot, FUN = mean, data = M24)
M48.agg <- aggregate(. ~ country_plot, FUN = mean, data = M48)
OFH.agg <- aggregate(. ~ country_plot, FUN = mean, data = OFH)

soc.tog$M01.C <- M01.agg$SOC[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.C <- M12.agg$SOC[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.C <- M24.agg$SOC[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.C <- M48.agg$SOC[match(soc.tog$country_plot, M48.agg$country_plot)]
soc.tog$OFH.C <- OFH.agg$OFH.SOC[match(soc.tog$country_plot, OFH.agg$country_plot)]

soc.tog$M01.bd <- M01.agg$BD[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.bd <- M12.agg$BD[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.bd <- M24.agg$BD[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.bd <- M48.agg$BD[match(soc.tog$country_plot, M48.agg$country_plot)]

soc.tog$M01.coarse.f <- M01.agg$Coarse[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.coarse.f <- M12.agg$Coarse[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.coarse.f <- M24.agg$Coarse[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.coarse.f <- M48.agg$Coarse[match(soc.tog$country_plot, M48.agg$country_plot)]

#Now make any value less than 0 NA
soc.tog[soc.tog < 0] <- NA

#Calculate mineral horizon soil C stock at respective cm depth
#Bulk density measurement is in kg/m3 and multiply by 0.001 to be g/cm3
#Remove the coarse fragment volume (%) by removing the fraction of density in BD that is coarse
#C measurements are g/kg and multiple by 0.1 to be a %
soc.tog$M01.C.stock <- (0.1*soc.tog$M01.C)*(soc.tog$M01.bd*0.001)*10
soc.tog$M01.C.stock.w.o.c <- (0.1*soc.tog$M01.C)*((soc.tog$M01.bd-(soc.tog$M01.bd*(soc.tog$M01.coarse.f*.01)))*0.001)*10

soc.tog$M12.C.stock <- (0.1*soc.tog$M12.C)*(soc.tog$M12.bd*0.001)*10
soc.tog$M12.C.stock.w.o.c <- (0.1*soc.tog$M12.C)*((soc.tog$M12.bd-(soc.tog$M12.bd*(soc.tog$M12.coarse.f*.01)))*0.001)*10

soc.tog$M24.C.stock <- (0.1*soc.tog$M24.C)*(soc.tog$M24.bd*0.001)*10
soc.tog$M24.C.stock.w.o.c <- (0.1*soc.tog$M24.C)*((soc.tog$M24.bd-(soc.tog$M24.bd*(soc.tog$M24.coarse.f*.01)))*0.001)*10

soc.tog$M48.C.stock <- (0.1*soc.tog$M48.C)*(soc.tog$M48.bd*0.001)*10
soc.tog$M48.C.stock.w.o.c <- (0.1*soc.tog$M48.C)*((soc.tog$M48.bd-(soc.tog$M48.bd*(soc.tog$M48.coarse.f*.01)))*0.001)*10

#Now add the SOC stock estimates to the tree growth and other ICP data.file data file
clay.df <- data.frame(clay = c(som.data$part_size_clay, fleck.data$part_size_clay),
country_plot = c(som.data$country_plot, fleck.data$country_plot),
depth = c(som.data$code_layer, fleck.data$code_layer))
clay.df <- clay.df[!grepl(-999999, clay.df$clay),] #Remove the missing fleck values represented as -999999.0
clay.df <- clay.df[!is.na(clay.df$clay), ]

#Now split by layer
clay.df.split <- split(clay.df, clay.df$depth)
soc.tog$clay.min <- clay.df.split$M01$clay[match(soc.tog$country_plot, clay.df.split$M01$country_plot)]

#Now add clay measurements from soilgrids
icp.dat <- icp.dat.age
icp.dat$lat_long <- paste(icp.dat$lat, icp.dat$long, sep = '_')
soil.grids.data$lat_long <- paste(soil.grids.data$Latitude, soil.grids.data$Longitude, sep = '_')
icp.dat <- merge(icp.dat, soil.grids.data, by = 'lat_long')

#Now merge the icp.dat dataset with the SOC/clay dataset
icp.dat <- merge(icp.dat, soc.tog, by = 'country_plot')
icp.dat$clay_0.5cm_mean <- icp.dat$clay_0.5cm_mean/10

#Add matching var for soil ph and then split by horizon
soil.ph.lab.meas$country_plot <- paste(soil.ph.lab.meas$Country, soil.ph.lab.meas$Plot, sep = '_')
soil.ph.lab.meas.split.hor <- split(soil.ph.lab.meas, soil.ph.lab.meas$Soil.Horizon)
icp.dat$M01.pH <- replace(icp.dat$M01.pH, which(icp.dat$M01.pH < 0), NA)
icp.dat$OFH.pH <- icp.dat$Organic.OFH.pH
icp.dat$OFH.pH <- replace(icp.dat$OFH.pH, which(icp.dat$OFH.pH < 0), NA)

#Now let's coalese the two soil pH in mineral horizon columns to have one measure
icp.dat$soil.ph.lab.meas <- soil.ph.lab.meas.split.hor$Min$pH[match(icp.dat$country_plot, soil.ph.lab.meas.split.hor$Min$country_plot)]
icp.dat$Mineral.soil.pH <- coalesce(icp.dat$M01.pH, icp.dat$soil.ph.lab.meas)

#Check the correlations to see which need splines
org.factors <- c("OFH.C", "mg_C_ha_yr", "N_dep_2019", "MAT", "MAP",
                 "OFH.pH", "clay_0.5cm_mean", "stem.count", "Stand.age")
org.files <- icp.dat[,org.factors]
pairs(log(org.files), pch = 19, lower.panel = NULL)

#Now look to see how correlated tree growth is with soil carbon stocks
#Organic horizon
org.gam.growth <- gam(log(OFH.C) ~ 
                        log(mg_C_ha_yr) + #tree growth factor
                        log(N_dep_2019) + #Nitrogen variables
                        MAT + MAP + #Climate variables
                        OFH.pH + #soil variables
                        s(stem.count, k = 3) +  #stand variables
                        Tree_type + Stand.age,
                        data = icp.dat, method = 'REML', na.action = 'na.omit')

#Diagnostics
qqnorm(org.gam.growth$residuals) #Pretty good
shapiro.test(org.gam.growth$residuals) #Pretty good
gam.check(org.gam.growth) #Perfect
summary(org.gam.growth)
samvif(org.gam.growth)

#Organic horizon
org.gam.growth <- gam(log(OFH.C) ~ 
                        log(mass.2) + #tree growth factor
                        log(N_dep_2019) + #Nitrogen variables
                        MAT + MAP + #Climate variables
                        OFH.pH + #soil variables
                        s(stem.count, k = 3) +  #stand variables
                        Tree_type + Stand.age,
                        data = icp.dat, method = 'REML', na.action = 'na.omit')

#Diagnostics
qqnorm(org.gam.growth$residuals) #Pretty good
shapiro.test(org.gam.growth$residuals) #Pretty good
gam.check(org.gam.growth) #Perfect
summary(org.gam.growth)
samvif(org.gam.growth)

#Check correlations for splines for the mineral horizon
min.factors <- c("M01.C.stock.w.o.c", "mg_C_ha_yr", "N_dep_2019", "MAT", "MAP",
                 "Mineral.soil.pH", "clay_0.5cm_mean", "stem.count", "Stand.age")
min.files <- icp.dat[,min.factors]
pairs(log(min.files), pch = 19, lower.panel = NULL)

#Mineral horizon
min.gam.growth <- gam(log(M01.C.stock.w.o.c) ~ #Mineral SOC stocks
                        log(mg_C_ha_yr) + #carbon stocks
                        log(N_dep_2019) + #Nitrogen variables
                        MAT + MAP + #Climate variables
                        s(Mineral.soil.pH, k = 3) + clay_0.5cm_mean + #soil variables
                        stem.count +  #stand variables
                        Tree_type + Stand.age,
                        data = icp.dat, method = 'REML', na.action = 'na.omit')

#Diagnostics
qqnorm(min.gam.growth$residuals) #Pretty good
shapiro.test(min.gam.growth$residuals) #Pretty good
gam.check(min.gam.growth) #Perfect
summary(min.gam.growth)
samvif(min.gam.growth)

####################################
###########Tree biomass#############
####################################
min.factors <- c("M01.C.stock.w.o.c", "mass.2", "N_dep_2019", "MAT", "MAP",
                 "Mineral.soil.pH", "clay_0.5cm_mean", "stem.count", "Stand.age")
min.files <- icp.dat[,min.factors]
pairs(log(min.files), pch = 19, lower.panel = NULL)

min.gam.growth <- gam(log(M01.C.stock.w.o.c) ~ #Mineral SOC stocks
                        log(mass.2) + #carbon stocks
                        log(N_dep_2019) + #Nitrogen variables
                        MAT + MAP + #Climate variables
                        s(Mineral.soil.pH, k = 3) + clay_0.5cm_mean + #soil variables
                        stem.count +  #stand variables
                        Tree_type + Stand.age,
                        data = icp.dat, method = 'REML', na.action = 'na.omit')

#Diagnostics
qqnorm(min.gam.growth$residuals) #Pretty good
shapiro.test(min.gam.growth$residuals) #Pretty good
gam.check(min.gam.growth) #Perfect
summary(min.gam.growth)
samvif(min.gam.growth)

#Make visualization
soc.x.tree.growth.min <- ggplot(icp.dat, aes(x = log(mg_C_ha_yr), y = log(M01.C.stock))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20,  show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(y = expression(paste("SOC stock log(t C h", a^{-1}, ")")), 
       x = expression(paste("Tree growth log(t C h", a^{-1}, " y", r^{-1}, ")")), color = "Tree type")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(plot.margin=grid::unit(c(0,0.1,0.1,0.1), "cm"))+ #+ #T,r,b,l
  theme(axis.text.x = element_text(size = 6, color = "black"))+
  theme(axis.text.y = element_text(size = 6, color = "black"))+
  theme(axis.title.x = element_text(size = 6, color = "black"))+
  theme(axis.title.y = element_text(size = 6, color = "black"))+
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "top")+
  theme(aspect.ratio = 1)

#make version with just data from our observations of the microbiome
icp.dat$microbiome_pair <- icp.envt$Sample[match(icp.dat$country_plot, icp.envt$country_plot)]
icp.dat.2 <- icp.dat[!is.na(icp.dat$microbiome_pair), ]

summary(lm(log(icp.dat.2$M01.C.stock.w.o.c) ~ log(icp.dat.2$mass.2)))
summary(lm(log(icp.dat.2$M01.C.stock) ~ log(icp.dat.2$mass.2)))
summary(lm(log(icp.dat.2$M01.C.stock.w.o.c) ~ log(icp.dat.2$mg_C_ha_yr)))
summary(lm(log(icp.dat.2$M01.C.stock) ~ log(icp.dat.2$mg_C_ha_yr)))

min.alone <- ggplot(icp.dat.2, aes(x = log(mg_C_ha_yr), y = log(M01.C.stock.w.o.c))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20,  show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(y = expression(paste("SOC stock log(t C h", a^{-1}, ")")), 
       x = expression(paste("Tree growth log(t C h", a^{-1}, " y", r^{-1}, ")")), color = "Tree type")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(axis.text.x = element_text(size = 7, color = "black"))+
  theme(axis.text.y = element_text(size = 7, color = "black"))+
  theme(axis.title.x = element_text(size = 8, color = "black"))+
  theme(axis.title.y = element_text(size = 8, color = "black"))+
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "top")+
  theme(aspect.ratio = 1)

min.alone <- ggplot(icp.dat.2, aes(x = log(mass.2), y = log(M01.C.stock.w.o.c))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20,  show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(y = expression(paste("SOC stock log(t C h", a^{-1}, ")")), 
       x = expression(paste("Tree growth log(t C h", a^{-1}, " y", r^{-1}, ")")), color = "Tree type")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(axis.text.x = element_text(size = 7, color = "black"))+
  theme(axis.text.y = element_text(size = 7, color = "black"))+
  theme(axis.title.x = element_text(size = 8, color = "black"))+
  theme(axis.title.y = element_text(size = 8, color = "black"))+
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "top")+
  theme(aspect.ratio = 1)
