#Figure 1 make ---

#We will first make a map and then a series of scatter plots

#Clear environment, source file paths, load packages
rm(list = ls(all.names = TRUE))
source('paths.R')

require(ggplot2)
require(ggpmisc)
require(vegan)
require(ggrepel)
require(corrplot)
require(grid)
require(reshape2)
require(rworldmap)
require(rworldxtra)
require(mapdata)
require(cowplot)
require(psych)
require(mgcv)
require(dplyr)
require(sf)
require(rnaturalearth)
require(rnaturalearthdata)
require(ggspatial)

#Upload the data files
#icp.dat <- readRDS(icp.plot.level.envt.matrix.processed.string)
icp.dat <- readRDS(icp.date.with.age.string)
fungi.dat <- readRDS(community.alpha.beta.processed.string)
icp.envt <- read.csv(envt.file.string)
icp.envt$country_plot <- paste(icp.envt$Country, icp.envt$Plot, sep = '_')
tree.fun.org <- readRDS(biomass.growth.organic.horizon.microbiome.string)
tree.fun.min <- readRDS(biomass.growth.mineral.horizon.microbiome.string)
soil.ph.lab.meas <- read.csv(soil.ph.measurements.string) #Add the lab measured soil ph values
soil.grids.data <- read.csv(soil.grids.data.string)
fleck.data <- read.csv(fleck.soil.data.string)
fleck.data$country_plot <- paste(fleck.data$code_country, fleck.data$code_plot, sep = '_')
som.data <- read.csv(soil.org.matter.data.string, sep = ';')
som.data$country_plot <- paste(som.data$code_country, som.data$code_plot, sep = '_')

#Now split these by soil depth
som.db.depth <- split(som.data, som.data$code_layer)
fleck.data.depth <- split(fleck.data, fleck.data$code_layer)

#Now make a df with country_plot and populate this with the depth variables
#key
#M01 = 0-10 cm
#M12 = 10-20 cm
#M24 = 20- 40 cm
#M48 = 40-80 cm
soc.tog <- data.frame(country_plot= unique(c(fleck.data$country_plot, som.data$country_plot)))

M01 = data.frame(SOC = c(som.db.depth$M01$organic_carbon_total, fleck.data.depth$M01$organic_carbon_total), 
                 country_plot = c(som.db.depth$M01$country_plot, fleck.data.depth$M01$country_plot),
                 BD = c(som.db.depth$M01$bulk_density, fleck.data.depth$M01$bulk_density),
                 Coarse = c(som.db.depth$M01$coarse_fragment_vol, fleck.data.depth$M01$coarse_fragment_vol))
M12 = data.frame(SOC = c(som.db.depth$M12$organic_carbon_total, fleck.data.depth$M12$organic_carbon_total), 
                 country_plot = c(som.db.depth$M12$country_plot, fleck.data.depth$M12$country_plot),
                 BD = c(som.db.depth$M12$bulk_density, fleck.data.depth$M12$bulk_density),
                 Coarse = c(som.db.depth$M12$coarse_fragment_vol, fleck.data.depth$M12$coarse_fragment_vol))
M24 = data.frame(SOC = c(som.db.depth$M24$organic_carbon_total, fleck.data.depth$M24$organic_carbon_total), 
                 country_plot = c(som.db.depth$M24$country_plot, fleck.data.depth$M24$country_plot),
                 BD = c(som.db.depth$M24$bulk_density, fleck.data.depth$M24$bulk_density),
                 Coarse = c(som.db.depth$M24$coarse_fragment_vol, fleck.data.depth$M24$coarse_fragment_vol))
M48 = data.frame(SOC = c(som.db.depth$M48$organic_carbon_total, fleck.data.depth$M48$organic_carbon_total), 
                 country_plot = c(som.db.depth$M48$country_plot, fleck.data.depth$M48$country_plot),
                 BD = c(som.db.depth$M48$bulk_density, fleck.data.depth$M48$bulk_density),
                 Coarse = c(som.db.depth$M48$coarse_fragment_vol, fleck.data.depth$M48$coarse_fragment_vol))

#Add the organic horizon
OFH = data.frame(SOC = c(som.db.depth$OFH$organic_carbon_total, fleck.data.depth$OFH$organic_carbon_total), 
                 country_plot = c(som.db.depth$OFH$country_plot, fleck.data.depth$OFH$country_plot),
                 Weight = c(som.db.depth$OFH$organic_layer_weight, fleck.data.depth$OFH$organic_layer_weight))

OFH$OFH.SOC <- ((0.1*OFH$SOC)*OFH$Weight)*0.1 #% C x mass  (kg/m2) so multiply by 0.1 to get to t/ha
OFH$OFH.SOC <- replace(OFH$OFH.SOC, which(OFH$OFH.SOC < 0), NA)

M01.agg <- aggregate(. ~ country_plot, FUN = mean, data = M01)
M12.agg <- aggregate(. ~ country_plot, FUN = mean, data = M12)
M24.agg <- aggregate(. ~ country_plot, FUN = mean, data = M24)
M48.agg <- aggregate(. ~ country_plot, FUN = mean, data = M48)
OFH.agg <- aggregate(. ~ country_plot, FUN = mean, data = OFH)

soc.tog$M01.C <- M01.agg$SOC[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.C <- M12.agg$SOC[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.C <- M24.agg$SOC[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.C <- M48.agg$SOC[match(soc.tog$country_plot, M48.agg$country_plot)]
soc.tog$OFH.C <- OFH.agg$OFH.SOC[match(soc.tog$country_plot, OFH.agg$country_plot)]

soc.tog$M01.bd <- M01.agg$BD[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.bd <- M12.agg$BD[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.bd <- M24.agg$BD[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.bd <- M48.agg$BD[match(soc.tog$country_plot, M48.agg$country_plot)]

soc.tog$M01.coarse.f <- M01.agg$Coarse[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.coarse.f <- M12.agg$Coarse[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.coarse.f <- M24.agg$Coarse[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.coarse.f <- M48.agg$Coarse[match(soc.tog$country_plot, M48.agg$country_plot)]

#Now make any value less than 0 NA
soc.tog[soc.tog < 0] <- NA

#Calculate mineral horizon soil C stock at respective cm depth
#Bulk density measurement is in kg/m3 and multiply by 0.001 to be g/cm3
#Remove the coarse fragment volume (%) by removing the fraction of density in BD that is coarse
#C measurements are g/kg and multiple by 0.1 to be a %
soc.tog$M01.C.stock <- (0.1*soc.tog$M01.C)*(soc.tog$M01.bd*0.001)*10
soc.tog$M01.C.stock.w.o.c <- (0.1*soc.tog$M01.C)*((soc.tog$M01.bd-(soc.tog$M01.bd*(soc.tog$M01.coarse.f*.01)))*0.001)*10

soc.tog$M12.C.stock <- (0.1*soc.tog$M12.C)*(soc.tog$M12.bd*0.001)*10
soc.tog$M12.C.stock.w.o.c <- (0.1*soc.tog$M12.C)*((soc.tog$M12.bd-(soc.tog$M12.bd*(soc.tog$M12.coarse.f*.01)))*0.001)*10

soc.tog$M24.C.stock <- (0.1*soc.tog$M24.C)*(soc.tog$M24.bd*0.001)*10
soc.tog$M24.C.stock.w.o.c <- (0.1*soc.tog$M24.C)*((soc.tog$M24.bd-(soc.tog$M24.bd*(soc.tog$M24.coarse.f*.01)))*0.001)*10

soc.tog$M48.C.stock <- (0.1*soc.tog$M48.C)*(soc.tog$M48.bd*0.001)*10
soc.tog$M48.C.stock.w.o.c <- (0.1*soc.tog$M48.C)*((soc.tog$M48.bd-(soc.tog$M48.bd*(soc.tog$M48.coarse.f*.01)))*0.001)*10


#And merge with icp.dat

#Now add this to the ICP.dat dataframe
icp.dat <- merge(icp.dat, soc.tog, sep = 'country_plot')


###
#
#
#
###
#Merge icp.envt and the fungal data
fungi.dat$File_name <- row.names(fungi.dat)
icp.envt.fungi <- merge(icp.envt, fungi.dat, by = 'File_name') 

#Add the sample name from icp.envt to icp.dat so we can merge with the fungal community
#Now merge the fungal data with icp.dat
tree.fun <- merge(icp.dat, icp.envt.fungi, by = 'country_plot', all = TRUE)
tree.fun <- tree.fun[!is.na(tree.fun$PCoA1),]
tree.fun <- tree.fun[!is.na(tree.fun$Tree_type),]

########################################################################
###########################PANEL A IS THE MAP###########################
########################################################################

#Fix lat, long values
tree.fun$Latitude <- tree.fun$lat
tree.fun$Longitude <- tree.fun$long
tree.fun.2 <- st_as_sf(tree.fun, coords = c("Longitude", "Latitude"), crs = 4326, agr = "constant")

#Get the world map
world <- ne_countries(scale = "medium", returnclass = "sf")

#Make the map
panel.a <- ggplot(data = world) +
  geom_sf()+
  geom_sf(data = tree.fun.2, aes(color = Tree_type), size = 2, shape = 20)+
  coord_sf(xlim = c(-10, 32), ylim = c(35.5, 70), expand = FALSE)+
  theme(panel.grid.major = element_line(color = gray(0.5), linetype = "dashed", 
                                        size = 0.5),
         panel.background = element_rect(fill = "white", color = "black", linewidth = 1))+
  theme(panel.border = element_rect(fill = NA, color = "black", linewidth = 1))+
  theme(legend.position = "top")+
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  labs(color = )+
  theme(legend.key=element_rect(fill="white"))+ #make legend fill white
  theme(axis.text.x = element_text(size = 6, color = "black"))+
  theme(axis.text.y = element_text(size = 6, color = "black"))+
  theme(axis.title.x = element_text(size = 6, color = "black"))+
  theme(axis.title.y = element_text(size = 6, color = "black"))

#####################################################################################################
#####################################################################################################
########Now make panel showing correlations between the SOC and tree growth and tree biomass#########
#####################################################################################################
#####################################################################################################

#make version with just data from our observations
icp.dat$microbiome_pair <- icp.envt$Sample[match(icp.dat$country_plot, icp.envt$country_plot)]
icp.dat.2 <- icp.dat[!is.na(icp.dat$microbiome_pair), ]

panel.b <- ggplot(icp.dat.2 , aes(x = log(mass.2), y = log(M01.C.stock.w.o.c))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(y = expression(paste("SOC stock log(t C h", a^{-1}, ")")), 
       x = expression(paste("Tree biomass log(t C h", a^{-1}, ")")), color = "Tree type")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.text = element_text(size = 10))

panel.c <- ggplot(icp.dat.2, aes(x = log(mg_C_ha_yr), y = log(M01.C.stock))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(y = expression(paste("SOC stock log(t C h", a^{-1}, ")")), 
       x = expression(paste("Tree growth log(t C h", a^{-1}, " y", r^{-1}, ")")), color = "Tree type")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.text = element_text(size = 10))+
  theme(axis.text.y = element_text(color = "white"))

#Get summary stats
summary(lm(log(icp.dat.2$mg_C_ha_yr) ~ log(icp.dat.2$M01.C.stock.w.o.c)))
summary(lm(log(icp.dat.2$mass.2) ~ log(icp.dat.2$M01.C.stock.w.o.c)))

#####################################################################################################
#####################################################################################################
#####Now make panel showing correlations between the microbiome and tree growth and tree biomass#####
#####################################################################################################
#####################################################################################################
panel.d <- ggplot(tree.fun.min, aes(x = PCoA1, y = log(mass.2))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(x = "Fungal composition (PCoA1)", 
       y = expression(paste("Tree biomass log(t C h", a^{-1}, ")")), fill = "")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.title = element_blank())+
  theme(axis.text = element_text(size = 8))

panel.e <- ggplot(tree.fun.min, aes(x = Bacteria_PCoA1, y = log(mass.2))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  labs(x = "Bacterial composition (PCoA1)", 
       y = expression(paste("Tree biomass log(t C h", a^{-1}, ")")), fill = "")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.title = element_blank())+
  theme(axis.text = element_text(size = 8))

panel.f <- ggplot(tree.fun.min, aes(x = Richness, y = log(mass.2))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(x = "Fungal richness (# of OTUS)", 
       y = expression(paste("Tree biomass log(t C h", a^{-1}, ")")), fill = "")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.title = element_blank())+
  theme(axis.text = element_text(size = 8))

panel.g <- ggplot(tree.fun.min, aes(x = Bacteria_Richness, y = log(mass.2))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  # geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(x = "Bacterial richness (# of OTUs)", 
       y = expression(paste("Tree biomass log(t C h", a^{-1}, ")")), fill = "")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.title = element_blank())+
  theme(axis.text = element_text(size = 8))

#Now make panel showing correlations between SOC and tree growth and tree biomass
panel.h <- ggplot(tree.fun.min, aes(x = PCoA1, y = log(mg_C_ha_yr))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(x = "Fungal composition (PCoA1)", 
       y = expression(paste("Tree growth log(t C h", a^{-1}," y", r^{-1}, ")")), fill = "")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.title = element_blank())+
  theme(axis.text = element_text(size = 8))

panel.i <- ggplot(tree.fun.min, aes(x = Bacteria_PCoA1, y = log(mg_C_ha_yr))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  labs(x = "Bacterial composition (PCoA1)", 
       y = expression(paste("Tree growth log(t C h", a^{-1}," y", r^{-1}, ")")), fill = "")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.title = element_blank())+
  theme(axis.text = element_text(size = 8))

panel.j <- ggplot(tree.fun.min, aes(x = Richness, y = log(mg_C_ha_yr))) +
  geom_point(aes(color = Tree_type),stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  geom_smooth(method = 'lm', formula = y ~ x, color = "black", show.legend = FALSE) +
  labs(x = "Fungal richness (# of OTUS)", 
       y = expression(paste("Tree growth log(t C h", a^{-1}," y", r^{-1}, ")")), fill = "")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.title = element_blank())+
  theme(axis.text = element_text(size = 8))

panel.k <- ggplot(tree.fun.min, aes(x = Bacteria_Richness, y = log(mg_C_ha_yr))) +
  geom_point(aes(color = Tree_type), stat = 'identity', size = 2, pch = 20, show.legend = FALSE)+
  labs(x = "Bacterial richness (# of OTUs)", 
       y = expression(paste("Tree growth log(t C h", a^{-1}," y", r^{-1}, ")")), fill = "")+
  scale_fill_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  scale_linetype_manual(values = c("solid", "blank"))+
  theme_bw() +
  theme(strip.background = element_blank(), strip.text = element_blank())+
  theme(axis.line = element_line(colour = "black"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank())+
  theme(legend.position = "bottom")+
  theme(aspect.ratio = 1)+
  theme(axis.title = element_blank())+
  theme(axis.text = element_text(size = 8))

#Get summary stats
cor.test(log(tree.fun.min$mg_C_ha_yr), tree.fun.min$PCoA1, use='complete.obs')
cor.test(log(tree.fun.min$mass.2), tree.fun.min$PCoA1, use='complete.obs')
cor.test(log(tree.fun.min$mg_C_ha_yr), tree.fun.min$Richness, use='complete.obs')
cor.test(log(tree.fun.min$mass.2), tree.fun.min$Richness, use='complete.obs')
cor.test(log(tree.fun.min$mg_C_ha_yr), tree.fun.min$Bacteria_PCoA1, use='complete.obs')
cor.test(log(tree.fun.min$mass.2), tree.fun.min$Bacteria_PCoA1, use='complete.obs')
cor.test(log(tree.fun.min$mg_C_ha_yr), tree.fun.min$Bacteria_Richness, use='complete.obs')
cor.test(log(tree.fun.min$mass.2), tree.fun.min$Bacteria_Richness, use='complete.obs')


#Save these as the outputs
row.1 <- plot_grid(panel.a, plot_grid(panel.b, panel.c, ncol = 2), ncol = 2, rel_widths = c(0.5, 1))
row.2 <- plot_grid(panel.d, panel.e, panel.f, panel.g, panel.h, panel.i, panel.j, panel.k, axes = "vh", align = "tblr", ncol = 4)

